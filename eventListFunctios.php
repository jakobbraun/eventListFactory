<?php
require 'vendor/autoload.php';

//usage: printEvents(array("bla"),5);

use ICal\ICal;

$ical   = new ICal("./MYical.ics");
$events = $ical->events(); 

date_default_timezone_set($ical->calendarTimeZone());

if(count($events) == 0){
  fwrite(STDERR, "error!!! ical invalid\n");
  exit(1);
}

echo "<?php header('Access-Control-Allow-Origin: *'); ?>\n\r";

function printEvents($filters = array(),$max = 0, $includeLoc = false){
    global $ical, $events;
    $printCount = 0;

    echo "<table>";
    foreach ($events as $event) {
        if($max != 0 && $printCount >= $max){
            $printCount++;
            break;
	}
        
        if($ical->iCalDateToUnixTimestamp($event->dtend) < time())
            continue;
        
        if(count($filters) == 0){
            printEvent($event, $includeLoc);
            $printCount++;
        }
        else{
            foreach($filters as $filter){
                if(preg_match("/\[[\s\S]*" . $filter . "[\s\S]*\]/",$event->summary)){
                    printEvent($event, $includeLoc);
                    $printCount++;
                    break;
                }
            }
        }
    }
    echo "</table>";
    return $printCount > $max;
}

function printEvent($event,$includeLoc){
    global $ical;
    $line = "<tr>";

    $begin = date_create();
    date_timestamp_set($begin,$ical->iCalDateToUnixTimestamp($event->dtstart));
    $line .= "<td>" . $begin->format('d.m.') . "</td>";
    
    $end = date_create();
    date_timestamp_set($end,$ical->iCalDateToUnixTimestamp($event->dtend) - 1);

    if($begin->format('m') != $end->format('m') || $begin->format('d') != $end->format('d')){
      $line .= "<td style='width:10px; text-align:center;'> - </td><td>" . $end->format('d.m.') .  "</td>";
    }
    else{
      $line .= "<td></td><td></td>";
    }
    
    $name =    $event->summary;
    
    $name = preg_replace("/\[[\s\S]*\]/","",$name);

    if($includeLoc && $event->location != ""){
      $name .= " - " . $event->location;
    }
    
    preg_match("/(https?:\/\/[^\s]+)/", $event->description, $output_array);
    if(count($output_array) > 0){
        $link = str_replace("\\n","",$output_array[0]);
        $link = str_replace("\\r","",$link);
        $name = "<a href='" . $link . "'>" . $name . "</a>";
    }

    $line .= "<td style='width:10px;'></td><td>" . $name . "</td></tr>";

    echo $line . "\n";
}

?>
