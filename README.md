# EventList
Generates an event List from the owncloud calender

## Setup
First of all check out this repository:

Next, install dependencys using composer:

`docker run --rm -v $(pwd):/app composer/composer install`

Now set up your `docker-compose.yml`:
```
najuterminliste:
    build: .
    volumes:
     - ./data:/data/
     - ./templates:/root/templates
    environment:
     - ical_url=
     - ical_user=
     - ical_pw=
```

### Templates
to fit the output into your website you need to ceate one or more templates.
Therefore create or checkout a templates folder and mount it as a volume unsing docker-compose.yml.
Yout also can copy the example template from templatesEXAMPLE
